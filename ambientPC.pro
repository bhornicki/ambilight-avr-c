#-------------------------------------------------
#
# Project created by QtCreator 2017-05-15T21:20:51
#
#-------------------------------------------------

QT       += core gui serialport network


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
include(src/libqtscreen/libqtscreen.pri)

TARGET = ambient2serialGUI
TEMPLATE = app


SOURCES += \
        src/PC/main.cpp\
        src/PC/widget.cpp \
        src/PC/hcombobox.cpp

HEADERS  += \
        src/PC/widget.h \
        src/PC/hcombobox.h

FORMS    += \
        src/PC/widget.ui
