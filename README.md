# ambientScreen
This repo contains code for both PC and AVR Atmega8 microcontroller.

Screenshot is taken, data prepared and sent to Atmega via UART, then displayed on SK2812 40 LED strip behind monitor. 


## PC
**QT framework** is used!

Window shows screenshot used to calculate LED strip data. When window is inactive, preview will not refresh.
You can set up brightness divider, COM port to send data to, autonatic mode to set up fps count or take single screenshot.

Screen capture is always followed with sending data via COM port. USB - UART FT232 converter is used here.


## AVR
This part reads color data packets from UART. 
Values are translated from linear to gamma curve and stored until entire buffer(3 8-bit colors x 40 LEDs) is filled. 
Then interrupts are disabled and data is pushed to SK2812 (WS2812 chinese clone, better than original) led strip.

Connect PB1 to SK2812 data pin, UART RX to MCU TX and good 5v power supply.
Too low maximum current will result in broken maximum brightness - i.e. white will be red.

## TODO
- [AVR] move to other build system (preferably CMake)
- [PC] fix uneven UART delay between data bursts, fix broken frequency (10fps is kinda ok, 15fps is like 13 and 30 is like 10...)
- [PC] create config file
  - UART config (baud, parity...)
  - Remember previous/default COM port 
  - autoconnect function
  - automatic capture default fps count
- [PC] change brightness divider to slider
- [PC] ? change screen capturing lib
- [PC] change color data calculating alghoritm 
- [AVR&PC] use standard baud like 19200 and on startup negotiate higher value. 
  - [AVR] when too many transmission erros occured, switch to standard baud
