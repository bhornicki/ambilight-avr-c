TEMPLATE = app

AVR_MCU         = $$lower(atmega8)
AVR_FCPU        = "16000000UL"
AVR_PROGRAMMER  = usbasp
AVR_PORT        = usb

QT -=gui core

CONFIG += c++11
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR = build

TARGET = ambientAVR.pro

INCLUDEPATH += D:\P\Programowanie\avr_Toolchain\avr\include

DEFINES = __AVR_atmega8__

#C compiler
QMAKE_CC = D:\P\Programowanie\avr_Toolchain\bin\avr-gcc
QMAKE_CFLAGS += -c -g -Os -wnone -ffunction-sections -fdata-sections
QMAKE_CFLAGS += -MMD -mmcu=$$AVR_MCU -DF_CPU=$$AVR_FCPU

#C++ compiler
QMAKE_CXX = D:\P\Programowanie\avr_Toolchain\bin\avr-g++
QMAKE_CXXFLAGS += -c -g -Os -w  -ffunction-sections -fdata-sections
QMAKE_CXXFLAGS += -fno-exceptions -fno-threadsafe-statics
QMAKE_CXXFLAGS += -MMD -mmcu=$$AVR_MCU -DF_CPU=$$AVR_FCPU

# linker
QMAKE_LINK = D:\P\Programowanie\avr_Toolchain\bin\avr-gcc
QMAKE_LFLAGS = -w -Os -Wl,--gc-sections -mmcu=$$AVR_MCU
QMAKE_LIBS = -lm

#hex file
QMAKE_POST_LINK += D:\P\Programowanie\avr_Toolchain\bin\avr-objcopy -O ihex -j .text -j .data -S ${TARGET} ${TARGET}.hex


SOURCES += \
    src/AVR/main.c \
    src/AVR/ws2812/ws2812.c

HEADERS += \
    src/AVR/ws2812/ws2812.h
