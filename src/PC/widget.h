#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QScreen>
#include <QTimer>
#include <QDebug>
#include <QElapsedTimer>
#include "libqtscreen/screenshot_maker.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
public slots:
	void screenshot();
private slots:
    void on_combFreq_activated(int index);
	void screenshotReady();
private:
//	NQtScreen::TScreenShotMaker *Screener;
    Ui::Widget *ui;
    QElapsedTimer *howLong;
    QImage mainScreen;
    QTimer *timDelay;
};

#endif // WIDGET_H
