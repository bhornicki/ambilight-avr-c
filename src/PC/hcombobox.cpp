#include "hcombobox.h"
hComboBox::hComboBox(QWidget *parent){
    setParent(parent);
    labStanPtr=nullptr;
    serialPort=new QSerialPort(this);
    timr=new QTimer(this);
    connect(timr,SIGNAL(timeout()),this,SLOT(slothTimrOvf()));
    timr->setInterval(1000);
    connect(this,SIGNAL(activated(int)),this,SLOT(slothActivated()));
//    connect(this->serialPort,SIGNAL(readyRead()),this,SLOT(slothSerialReadyRead()));
//    serialSzukaj();


}
hComboBox::~hComboBox(){
    delete serialPort;
    delete timr;
}

//Dodaj porty do listy
void hComboBox::serialSzukaj(){
    this->clear();
    this->addItem("");
    for (int cnt = 0; cnt < QSerialPortInfo::availablePorts().count(); ++cnt) {
        addItem(QSerialPortInfo::availablePorts().at(cnt).portName());
    }
}

//nadpisanie wyświetlania listy; nie zeruj pozycji jeżeli nazwa jest na liście
void hComboBox::showPopup(){
    poprzPort=currentText();
    serialSzukaj();
    if(findText(poprzPort)!=-1)
        setCurrentText(poprzPort);
    QComboBox::showPopup();


}

/*
 *  Po wybraniu opcji jeżeli port otwarty
 *  i zmieniono nazwę - zamknij aktualny.
 *  następnie jeśli port zamknięty otwórz
 *  Czyż trudne? Według mnie nie - i już.
 */
void hComboBox::slothActivated(){
    if ((serialPort->isOpen())&&(currentText()!=poprzPort))
        serialDisconnect();

    if(!serialPort->isOpen())
        serialConnect();
}

/*
 *  gdy port otwarty sprawdzaj co sekundę
 *  czyżli nie został fizycznie odłączony
 *  jak tak, przeprowadź  zamykania rundę
 *  (QLabel i aktualny port podmieniony).
 */
void hComboBox::slothTimrOvf(){
    if(serialPort->error()) {
        serialDisconnect();
    }
}

/*
 *  Sprawdź, czy wybrane pole zawiera COM,
 *  czy nie jest jakimś cudem otwarty port
 *  Ustaw nazwę portu, w hCBoxie znajdź tą
 *  Jeżeli sezam otworzy, label pozytywny,
 *  albo rzuć portDisconnect i idź w czort
 *  Funkcja chyba nie zrobi sobie krzywdy.
 */
void hComboBox::serialConnect(){
    if(!currentText().contains("COM",Qt::CaseInsensitive)) return;
    if (serialPort->isOpen()) serialPort->close();
    serialPort->setPortName(currentText());
	serialPort->setBaudRate(1000000);
    if(serialPort->open(QIODevice::ReadWrite)){
        timr->start();
        if(labStanPtr!=nullptr) labStanPtr->setText(labStanText+"Połączono z "+serialPort->portName());
        emit portConnected();
    }
    else {
        setCurrentIndex(0);
        emit portDisconnected();
        if(labStanPtr!=nullptr) labStanPtr->setText(labStanText+"Błąd łączenia!");
    }
}

/*  Funkcja ta przejrzysta i prosta
 *  Zamyka serial i się nam ukazuje
 *  "Rozłączono" informacja radosna
 *  Emituje sygnał i pozycję zeruje
 *  timrOff, już błędu nie oczekuje
 */
void hComboBox::serialDisconnect(bool keepPos){
    if(labStanPtr!=nullptr) labStanPtr->setText(labStanText+"<font color='red'><b>Rozłączono</b></font>"); //jeżeli wywołało przerwanie
    if (serialPort->isOpen()) {
        serialPort->close();
        if(labStanPtr!=nullptr) labStanPtr->setText(labStanText+"Rozłączono");
    }
    timr->stop();
    glInMsg.clear();
    if(!keepPos) setCurrentIndex(0);
    emit portDisconnected();
}

void hComboBox::setLabelPtr(QLabel *label, QString constText){

    labStanPtr=label;
    labStanText=constText;
    if(labStanPtr!=nullptr) labStanPtr->setText(labStanText+"Nie połączono");
}


bool hComboBox::serialSend(QByteArray &data){
    uint8_t cnt=128;
    serialPort->write(data+"\r\n");
    while(--cnt){
        if(serialPort->waitForBytesWritten(10)) return 1;
    }
    return 0;
}

void hComboBox::slothSerialReadyRead(){

        glInMsg+=serialPort->readAll();

//      BOCZNIK

//        emit readLine(glInMsg);
//        glInMsg.clear();

        while (glInMsg.contains('\r')) glInMsg.remove('\r');

        int idx=glInMsg.indexOf('\n');
        while(idx>=0){
            emit readLine(glInMsg.left(idx));
            glInMsg.remove(0,idx+1);//usuń włącznie z \n
            idx=glInMsg.indexOf('\n');

        }
    }


