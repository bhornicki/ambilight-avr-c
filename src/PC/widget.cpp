#include "widget.h"
#include "ui_widget.h"
const uint8_t gammaArRay[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5,
        5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 9, 9, 9, 10, 10, 10, 11, 11,
        11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19, 19,
        20, 20, 21, 21, 22, 22, 23, 24, 24, 25, 25, 26, 27, 27, 28, 29, 29, 30,
        31, 32, 32, 33, 34, 35, 35, 36, 37, 38, 39, 39, 40, 41, 42, 43, 44, 45,
        46, 47, 48, 49, 50, 50, 51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63,
        64, 66, 67, 68, 69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86,
        87, 89, 90, 92, 93, 95, 96, 98, 99, 101, 102, 104, 105, 107, 109, 110,
        112, 114, 115, 117, 119, 120, 122, 124, 126, 127, 129, 131, 133, 135,
        137, 138, 140, 142, 144, 146, 148, 150, 152, 154, 156, 158, 160, 162,
        164, 167, 169, 171, 173, 175, 177, 180, 182, 184, 186, 189, 191, 193,
        196, 198, 200, 203, 205, 208, 210, 213, 215, 218, 220, 223, 225, 228,
        231, 233, 236, 239, 241, 244, 247, 249, 252, 255 };

Widget::Widget(QWidget *parent) : QWidget(parent), ui(new Ui::Widget) {
    ui->setupUi(this);
    timDelay=new QTimer(this);
	howLong=new QElapsedTimer;
//	Screener= new NQtScreen::TScreenShotMaker;
    ui->combPort->setLabelPtr(ui->labStanPortu);
	connect(timDelay,SIGNAL(timeout()),this,SLOT(screenshotReady()));
//	connect(Screener,SIGNAL(OnScreenshotReady()),this,SLOT(screenshotReady()));
//    connect(ui->combPort->serialPort,SIGNAL(readyRead()),this,SLOT(slothSerialReadyRead()));
}

Widget::~Widget(){
    timDelay->stop();
    ui->combPort->serialPort->waitForBytesWritten(-1);
    delete timDelay;
    delete ui;
    delete howLong;
//	delete Screener;
}

void Widget::screenshot(){
//	qDebug()<<"Call";
//Screener->MakeScreenshot();
emit screenshotReady();
}

void Widget::screenshotReady(){
	qDebug()<<"Screen";
	mainScreen=QGuiApplication::primaryScreen()->grabWindow(0).scaled(40,8).toImage();
    //    mainScreen=QGuiApplication::primaryScreen()->grabWindow(0).scaled(160,16,Qt::IgnoreAspectRatio,Qt::SmoothTransformation).scaled(40,4).toImage();

	if(Widget::isActiveWindow())
		ui->label->setPixmap(QPixmap::fromImage(mainScreen.scaled(420,180)));
    if(ui->combPort->serialPort->isOpen()){

        QByteArray bufor;
        int dzielnik=ui->combDzielnik->currentIndex();
        for (int i = 0; i < 40; ++i) {
            uint32_t dat=mainScreen.pixel(i,0);

			bufor.append((char)gammaArRay[((dat>>8)&0xff)>>dzielnik]*.8);      //g
            bufor.append((char)gammaArRay[((dat>>16)&0xff)>>dzielnik]);     //r
			bufor.append((char)gammaArRay[(dat&0xff)>>dzielnik]*.4);        //b
        }
//        howLong->restart();
        ui->combPort->serialSend(bufor);
//        if(!ui->combPort->serialPort->waitForReadyRead(80)) ui->combPort->serialDisconnect();
//        qDebug()<<howLong->elapsed()<<"send";

    }
}

void Widget::on_combFreq_activated(int index){
    if(index==0) timDelay->stop();
    else timDelay->start(1000/(ui->combFreq->currentText().remove(" fps").toShort()));
}

