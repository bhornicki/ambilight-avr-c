#ifndef HCOMBOBOX_H
#define HCOMBOBOX_H

#include <QComboBox>
#include <QtSerialPort>
#include <QLabel>

#include "widget.h"

class hComboBox : public QComboBox
{
    Q_OBJECT
public:
    explicit hComboBox(QWidget *parent = 0);
    virtual ~hComboBox();
    QSerialPort *serialPort;    //do dowolnego korzystania; Zaleca się obsługę wysyłania/odbioru
    void setLabelPtr(QLabel *label=nullptr, QString constText=""); //wyświetlaj informacje w postaci constText+"informacja"
    bool serialSend(QByteArray &data);
    void serialDisconnect(bool keepPos=0);
signals:
    void portConnected();
    void portDisconnected();
    void readLine(QString arg);


private:
    QTimer *timr;
    QString poprzPort;
    QLabel *labStanPtr;
    QString labStanText;
    QString glInMsg;

    virtual void showPopup();

    void serialConnect();
    void serialSzukaj();

private slots:
    void slothTimrOvf();
    void slothActivated();
    void slothSerialReadyRead();
};

#endif // HCOMBOBOX_H
