/*
 * main.c
 *
 *  Created on: 18.05.2017
 *      Author: Horochov PL
 */

//#include <util/delay.h>
//#include <avr/pgmspace.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>

#include "ws2812/ws2812.h"



//How many LEDs do You have behind screen
#define LED_cnt 40

typedef struct
{
	uint8_t g;
	uint8_t r;
	uint8_t b;
} GrbColor;

typedef union
{
	GrbColor color;
	uint8_t byte[3];
} Led;

volatile Led ledStrip[LED_cnt] = {};

volatile uint8_t dataIdx;

/*
 * gamma correction for LEDs
 */
const uint8_t gammaArRay[] =
{
	0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4,
	4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 9, 9, 9, 10, 10, 11,	11,
	11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19, 19, 20,
	20, 21, 21, 22, 23, 23, 24, 24, 25, 26, 26, 27, 28, 28, 29, 30, 30, 31, 32,
	33, 33, 34, 35, 36, 36, 37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 45, 46, 47,
	48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 67,
	68, 69, 70, 71, 72, 73, 75, 76, 77, 78, 79, 81, 82, 83, 85, 86, 87, 88, 90,
	91, 92, 94, 95, 97, 98, 99, 101, 102, 104, 105,	107, 108, 110, 111, 113,
	114, 116, 117, 119, 121, 122, 124, 125, 127, 129, 130, 132, 134, 135, 137,
	139, 141, 142, 144, 146, 148, 149, 151, 153, 155, 157, 159, 161, 162, 164,
	166, 168, 170, 172, 174, 176, 178, 180, 182, 184, 186, 188, 190, 192, 195,
	197, 199, 201, 203, 205, 208, 210, 212, 214, 217, 219, 221, 223, 226, 228,
	230, 233, 235, 237, 240, 242, 245, 247, 250, 252, 255
};

//void UART_putc(char data) {
//	while (!( UCSRA & (1 << UDRE)))
//		;
//	UDR = data;
//}

//ISR(TIMER0_OVF_vect) {
//	TCNT0 = 240;
//	if (timerMs) --timerMs;
//}


/*
 * UART receive interrupt
 */
ISR( USART_RXC_vect )
{
	//gamma translate data and save to the position
	*((volatile uint8_t*) ledStrip + dataIdx++) = gammaArRay[UDR];

	if (dataIdx >= LED_cnt * 3)
	{
		dataIdx = 0;
		ws2812_asm_send((void*) ledStrip, LED_cnt);
	}
}


/*
 * M A I N
 */

int main()
{
//	ws2812_asm_send((uint8_t*) ledStripe, LED_cnt);


	/*
	 * Set up UART
	 */
	//1MBaud
	UBRRH=0;
	UBRRL=0;
	//uncomment for 2 MBaud:
//	UCSRA |= (1<<U2X);

	//frame 8bit, 1 stop, no parity
	UCSRC = (1 << URSEL) | (1 << UCSZ0) | (1 << UCSZ1);
	UCSRB |= (1 << RXEN) | (1 << TXEN) | (1 << RXCIE);

//	/*		ADC		2,56v		*/
//	ADMUX |= (1 << REFS0) | (1 << REFS1) | (1 << ADLAR) | (1 << MUX0)
//			| (1 << MUX1);	//Vref 2,56; 8bit; ADC3(PC3)
//	ADCSRA |= (1 << ADEN);	//w��cz

//	/*		Timer0 	1ms/tick	*/
//	TCCR0 |= (1 << CS02) | (1 << CS00);	//1024 dzielnik
//	TIMSK |= (1 << TOIE0);	//przerwanie

	sei();
	while (1);

}
