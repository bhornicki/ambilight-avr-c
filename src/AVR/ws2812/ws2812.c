#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>

#include "ws2812.h"

/*
 * Push bytes describing colors to the diodes.
 * Data should be sorted in order: Green1, Red1, Blue1, Green2, Red2, Blue2...
 * Disable interrupts for stability, after process revert previous state.
 * This code is highly time-dependant.
 */
void ws2812_asm_send(void *data, uint16_t datlen) {

	uint8_t databyte = 0, cnt, pinLO = ~WS_PIN;
	WS_DIR |= WS_PIN;
	datlen *= 3;
	uint8_t sreg=SREG;
	cli();

	asm volatile(
			"		lds		%[cnt],%[ws_port]	\n\t"
			"		or		%[pinHI],%[cnt]		\n\t"
			"		and		%[pinLO],%[cnt]		\n\t"
			"mPTL%=:subi	%A6,1				\n\t"
			"		sbci	%B6,0				\n\t"
			"		brcs	exit%=				\n\t"
			"		ld		%[databyte],X+		\n\t"
			"		ldi		%[cnt],8			\n\t"

			"oPTL%=:sts		%[ws_port],	%[pinHI]\n\t"
			"		rjmp .+0					\n\t"

			"		lsl		%[databyte]			\n\t"
			"		brcs	.+2					\n\t"
			"		sts		%[ws_port],	%[pinLO]\n\t"
			"		rjmp .+0					\n\t"
			"		rjmp .+0					\n\t"

			"		dec		%[cnt]				\n\t"
			"		sts		%[ws_port],	%[pinLO]\n\t"
			"		breq	mPTL%=				\n\t"
			"		rjmp .+0					\n\t"

			"		rjmp	oPTL%=				\n\t"
			"exit%=:							\n\t"
			: [cnt]"=&d" (cnt)
			: [databyte]"r" (databyte), [ws_port]"M" (_SFR_MEM_ADDR(WS_PORT)),
			[pinHI]"r" (WS_PIN), [pinLO]"r" (pinLO), [data]"x" (data), [datlen]"r" (datlen)
	);
	SREG=sreg;
}
