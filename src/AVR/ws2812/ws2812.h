#ifndef WS2812_H_
#define WS2812_H_

#include <stdint.h>

/*
 * CONFIG
 */
#define WS_DIR DDRB
#define WS_PORT PORTB
#define WS_PIN (1<<PB1)


void ws2812_asm_send(void *data, uint16_t datlen);

#endif /* WS2812_H_ */
